/**
 * Created by wlh on 16/2/15.
 */

// <reference path="../lib/express.d.ts" />

import * as express from "express";

var app = express();

/**
 * @class MyClass 测试类
 * @example
 * ```
 * 这是一个测试注释
 * var myClass = new Class();
 * ```
 */
class MyClass{

}

var index : number = 0;
console.info("hello wolrd this is a test typescript!");

app.get("/", (req, res, next) => {
    res.send("hello world, request index " + index++);
});

app.listen(3000, function(err) {
    if (!err) {
        console.info("server run success!");
    }
});